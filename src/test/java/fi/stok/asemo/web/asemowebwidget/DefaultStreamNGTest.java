/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.asemowebwidget;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.testng.annotations.Test;

/**
 *
 * @author Juha Loukkola
 */
public class DefaultStreamNGTest {

    public DefaultStreamNGTest() {
    }

    @Test(enabled = false, groups = {"system_testing"})
    public void testGetServletInfo() throws MalformedURLException, IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {
        String serverUrl = "http://localhost:8181/test/";
        OAuthConsumer consumer = new CommonsHttpOAuthConsumer("token", "secret");
        consumer.setTokenWithSecret("720722c800e55daddc81320fcf4651f8", "0b947a659acd1eda5cfd2e276247ce40");

        OAuthProvider provider = new DefaultOAuthProvider(
                serverUrl + "request_token",
                serverUrl + "access_token",
                serverUrl + "authorize");

        HttpGet request = new HttpGet("http://localhost:8080/AsemoWebWidget/default_stream");
        consumer.sign(request);
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }

        httpClient.getConnectionManager().shutdown();
    }

}
