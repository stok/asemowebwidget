<%-- 
    Document   : graph_test_widget
    Created on : Oct 8, 2013, 6:18:46 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <script type="text/javascript" src="js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery/jquery-migrate-1.2.1.js"></script>
        <!--[if IE]>
        <script type="text/javascript" src="js/dygraph/excanvas.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/dygraph/dygraph-combined.js"></script>
        <script type="text/javascript" src="js/logView.js"></script>
    </head>
    <div id="graphdiv"></div>

    <script type="text/javascript">
        //<!CDATA[[

        function resize() {
            height = ($(window).height()) - 220;
            if (height < min_height) {
                height = min_height;
            }
            $("div#graphdiv").attr('style', 'width: 100%; height: ' + 200 + 'px;');
            if (lv) {
                lv.resize();
            }
        }

        function initLogView() {
            min_height = 200;
            var now = new Date();

            var container = document.getElementById("graphdiv");
            var stream = ${param.stream};
            var maxPoints = $(window).width() / 1.5;
            to = Math.round(now.getTime() / 1000);
            from = to - (3600 * ${!(empty param.hours) ? param.hours : 2});
            var url = "TestLogData";
            var xLabel = "Aika";
            var yLabel = "Arvo";
            var unit = "";
            lv = new LogView(container, stream, maxPoints, from, to, url, xLabel, yLabel, unit, null);
            lv.resolution = ${!(empty param.resolution) ? param.resolution : 1};
            resize();
            updateGraph();
            updateInterval = setInterval(updateGraph, 5000);
        }

        function updateGraph() {
            var now = new Date();
            lv.to = Math.round(now.getTime() / 1000);
            lv.from = lv.to - (3600 * ${!(empty param.hours) ? param.hours : 2});
            lv.roll();
            lv.update();
        }

        var updateInterval;
        $(window).ready(initLogView);
        $(window).resize(resize);

        window.onerror = function(desc, page, line, chr)
        {
            alert("Error: " + desc + " @" + page + ':' + line + ':' + chr);
        };
        //]]>
    </script>
</html>
