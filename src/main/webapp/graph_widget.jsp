<%-- 
    Document   : graph_widget
    Created on : Oct 30, 2013, 9:55:02 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <script type="text/javascript" src="js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery/jquery-migrate-1.2.1.js"></script>
        <!--[if IE]>
        <script type="text/javascript" src="js/dygraph/excanvas.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/dygraph/dygraph-combined.js"></script>
        <script type="text/javascript" src="oauth-js/sha1.js"></script>
        <script type="text/javascript" src="oauth-js/oauth.js"></script>
        <script type="text/javascript" src="js/logView.js"></script>
    </head>
    <div id="graphdiv"></div>

    <script type="text/javascript">
        //<!CDATA[[

        function resize() {
            height = ($(window).height()) - 220;
            if (height < min_height) {
                height = min_height;
            }
            $("div#graphdiv").attr('style', 'width: 100%; height: ' + 200 + 'px;');
            if (lv) {
                lv.resize();
            }
        }

        function initLogView() {
            min_height = 200;
            var now = new Date();

            var container = document.getElementById("graphdiv");
            if (!stream) {

            }
            var maxPoints = $(window).width() / 1.5;
            to = Math.round(now.getTime() / 1000);
            from = to - (3600 * ${!(empty param.hours) ? param.hours : 2});
            var url = "LogData";
            var xLabel = "Aika";
            var yLabel = "Arvo";
            var unit = "";
            lv = new LogView(container, stream, maxPoints, from, to, url, xLabel, yLabel, unit, null);
            lv.resolution = ${!(empty param.resolution) ? param.resolution : 2};

            resize();
            updateGraph();
            updateInterval = setInterval(updateGraph, 5000);
        }

        function updateGraph() {
            var now = new Date();
            lv.to = Math.round(now.getTime() / 1000);
            lv.from = lv.to - (3600 * ${!(empty param.hours) ? param.hours : 2});
            lv.roll();
            lv.update();
        }

        function getStream() {
            if (oauhtAccessor.consumerKey !== null
                    && oauhtAccessor.consumerSecret !== null
                    && oauhtAccessor.requestToken !== null
                    && oauhtAccessor.requestTokenSecret !== null) {
                var message = {
                    method: "GET", action: "https://asemo.fi/AsemoWebWidget/default_stream"
                };
                return pureOAuth(message, oauhtAccessor);
            }
        }

        function pureOAuth(message, accessor) {
            var requestBody = OAuth.formEncode(message.parameters);
            OAuth.completeRequest(message, accessor);
            var authorizationHeader = OAuth.getAuthorizationHeader("", message.parameters);
            return $.ajax({
                context: this,
                type: message.method,
                url: message.action,
                headers: {Authorization: authorizationHeader}
            })
        }

        var updateInterval;
        var oauhtAccessor = {
            consumerKey: "${param.consumer_key}",
            consumerSecret: "${param.consumer_secret}",
            token: "${param.token}",
            tokenSecret: "${param.token_secret}"
        };
        var stream = "${!(empty param.stream) ? param.stream : null}";
        if (stream) {
            $(window).ready(initLogView);
        } else {
            getStream().done(function(responce) {
                stream = responce.toString();
                $(window).ready(initLogView);
            });
        }

        $(window).resize(resize);

        window.onerror = function(desc, page, line, chr)
        {
            alert("Error: " + desc + " @" + page + ':' + line + ':' + chr);
        };
        //]]>
    </script>
</html>

