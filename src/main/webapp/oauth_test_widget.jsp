<%-- 
    Document   : widget
    Created on : Oct 4, 2013, 4:57:24 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="oauth-js/sha1.js"></script>
        <script type="text/javascript" src="oauth-js/oauth.js"></script>
        <title>Asemo widget</title>
    </head>
    <body>
        <h1>OAuth Test</h1>
        <script>
            var accessor = {
                consumerKey: "${param.data-consumer_key}", //LAXbqpUDutSRxpUg2abEYT78Zh9b8zfi",
                consumerSecret: "${param.data-consumer_secret}",//"E96Ef55QSRRutQ2SHrhzqtEb6dhqgpcX",
                token: "${param.data-token}", //"023729ef9fed3feb7b4dbba8c18c2811",
                tokenSecret: "${param.data-token_secret}", //"475057c42d0006e2c2241cf49ae9c211",
                serviceProvider: {
                    signatureMethod: "HMAC-SHA1",
                    echoURL: "https://asemo.fi/oauth-example-provider-20100601/echo" // "http://localhost:8080/oauth-provider/echo"
                }
            };
            var d = $.Deferred();
            consumeEchoService().done(function(responce) {
                alert(responce.toString());
            });

            function consumeEchoService() {
                if (accessor.consumerKey !== null
                        && accessor.consumerSecret !== null
                        && accessor.requestToken !== null
                        && accessor.requestTokenSecret !== null) {
                    var message = {
                        method: "GET", action: accessor.serviceProvider.echoURL
                    };
                    return pureOAuth(message, accessor);
                }
            }

            function pureOAuth(message, accessor) {
                var requestBody = OAuth.formEncode(message.parameters);
                OAuth.completeRequest(message, accessor);
                var authorizationHeader = OAuth.getAuthorizationHeader("", message.parameters);
                return $.ajax({
                    context: this,
                    type: message.method,
                    url: message.action,
                    headers: {Authorization: authorizationHeader}
                })
            }
        </script>
    </body>
</html>
