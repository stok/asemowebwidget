/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.asemowebwidget;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.OAuthService;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.oauth.OAuthException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Juha Loukkola
 */
@WebServlet(name = "LogData", urlPatterns = {"/LogData"})
public class LogData extends HttpServlet {

    @EJB
    private Business business;

    @EJB
    private OAuthService oauth;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String responseMessage = null;
        PrintWriter out = response.getWriter();

        try {
            // Identify the user performing this request.
//            User user = this.oauth.authenticateRequest(request);

            // Parse POST/URL parameters 
            //  - stream an integer representing an id of an stream
            //  - from an unix timestamp representing a time which from the data will be retrieved
            //  - to an unix timestamp representing a time to which the data will be retrieved
            //  - logLevel an integer representing optimal sampling period in miliseconds
            //  - format a string that defines the format in which the results will be returned.
            //      Currently supports JSON and CSV.
            //  - includeMissing a flag that defines if the data to be retruned should contain also missing data points
            // Get stream
            int streamId = Integer.parseInt(request.getParameter("stream"));
            Stream stream = this.business.getStream(streamId);

            // Check that the stream belongs to authenticated user
//            if (user.getUserSettings().getStreams().contains(stream)) {
//                throw new OAuthException("not authorized content");
//            }

            // Resolve meauring unit
            String unit = stream.getVariable().getType().getUnitOfMeasure();

            Date from = new Date(Long.parseLong(request.getParameter("from")) * 1000);
            // Parameter "to" is allowed to be not specified.
            Date to = request.getParameter("to") == null ? new Date() : new Date(Long.parseLong(request.getParameter("to")) * 1000);
            Long logLevel = Long.parseLong(request.getParameter("logLevel"));
            // Find most suitable loglevel
//                logLevel = business.getNearestLogLevel(logLevel);
            String format = request.getParameter("format");
            Boolean includeMissingData = request.getParameter("includeMissing").equals("true");

            // Get values for a stream                
            List<Value> values = this.business.getValues(streamId, logLevel, from, to);

            // TODO: Get timezone.
            // - add timezone property to the User entity class
            // - add option to specify timezone in /admin/UserSettings
            String timezone = "GMT+2";

            // Format the responce message.
            if (format.equals("json") || format.equals("JSON")) {
                responseMessage = formatResponseJSON(values, from, to, unit, timezone, new Date(), includeMissingData);
                response.setContentType("application/json");
            } else if (format.equals("cvs") || format.equals("CVS")) {
                responseMessage = formatResponseCSV(values);
                response.setContentType("text/html;charset=UTF-8");
            }

            if (responseMessage != null) {
                out.print(responseMessage);
            }

        } catch (Exception ex) {
            OAuthService.handleException(ex, request, response, false);
        } finally {
            out.close();
        }

    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String formatResponseCSV(List<Value> measurements) {
        String csv = "Aika,Energiankulutus\n";
        for (Value m : measurements) {
            csv = csv + m.getTimeMeasured() + "," + m.getValue() + "\r\n";
        }
        return csv;
    }

    private String formatResponseJSON(List<Value> measurements, Date from, Date to, String unit, String timeZone, Date serverTime, Boolean includeMissing) {
        // TODO: Add exception handling for situatien when mesusrements is empty
        Long last = null;
        JSONArray pointsArray = null;

        try {
            JSONObject jo = new JSONObject();
            // Construct "points" JSON array

            pointsArray = new JSONArray();
            JSONArray measurement;

            // Include the point describing the point at the start of the requested interval
            if (includeMissing) {
                measurement = new JSONArray();
                measurement.put(from.getTime());
                pointsArray.put(measurement);
            }

            if (!measurements.isEmpty()) {//if (measurements != null) {
                // Include all the datapoints within the interval
                for (Value m : measurements) {
                    measurement = new JSONArray();
                    measurement.put(m.getTimeMeasured());
                    measurement.put(m.getValue());
                    pointsArray.put(measurement);
                }
                last = measurements.get(measurements.size() - 1).getTimeMeasured();
            }

            // Include the point describing the point at the end of the requested interval
            if (includeMissing) {
                measurement = new JSONArray();
                measurement.put(to.getTime());
                pointsArray.put(measurement);
            }
            jo.put("points", pointsArray);
            jo.put("unit", unit);
            jo.put("tz", timeZone);
            jo.put("last", last);
            jo.put("server_time", serverTime.getTime() / 1000);
            String result = jo.toString();
            return result;
        } catch (JSONException ex) {
            Logger.getLogger(LogData.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
