/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.asemowebwidget;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Juha Loukkola
 */
@WebServlet(name = "TestLogData", urlPatterns = {"/TestLogData"})
public class TestLogData extends HttpServlet {

    @EJB
    private Business business;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // Parse POST/URL parameters
            //  - stream an integer representing an id of an stream
            //  - from an unix timestamp representing a time which from the data will be retrieved
            //  - to an unix timestamp representing a time to which the data will be retrieved
            //  - logLevel an integer representing optimal sampling period in miliseconds
            //  - format a string that defines the format in which the results will be returned.
            //      Currently supports JSON and CSV.
            //  - includeMissing a flag that defines if the data to be retruned should contain also missing data points
            int streamId = Integer.parseInt(request.getParameter("stream"));
            Date from = new Date(Long.parseLong(request.getParameter("from")) * 1000);
            // Parameter "to" is allowed to be not specified.
            Date to = request.getParameter("to") == null ? new Date() : new Date(Long.parseLong(request.getParameter("to")) * 1000);
            Long logLevel = Long.parseLong(request.getParameter("logLevel"));
            String format = request.getParameter("format");
            Boolean includeMissingData = request.getParameter("includeMissing").equals("true") ? true : false;
            
            String timezone = "GMT+2";
            String unit = "W";
            // Get values for a stream
            List<fi.stok.asemo.biz.ejb.entity.schema.Value> values = this.business.getValues(streamId, logLevel, from, to);
            //List<Value> values = this.generateLog(logLevel, from, to);
            
            String responseMessage = formatResponseJSON(values, from, to, unit, timezone, new Date(), includeMissingData);
            response.setContentType("application/json");
            
            // Print response
            if (responseMessage
                    != null) {
                PrintWriter out = response.getWriter();
                try {
                    out.print(responseMessage);
                } finally {
                    out.close();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TestLogData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String formatResponseJSON(List<Value> measurements, Date from, Date to, String unit, String timeZone, Date serverTime, Boolean includeMissing) {
        // TODO: Add exception handling for situatien when mesusrements is empty
        Long last = null;
        JSONArray pointsArray = null;

        try {
            JSONObject jo = new JSONObject();
            // Construct "points" JSON array

            pointsArray = new JSONArray();
            JSONArray measurement;

            // Include the point describing the point at the start of the requested interval
            if (includeMissing) {
                measurement = new JSONArray();
                measurement.put(from.getTime());
                pointsArray.put(measurement);
            }

            if (!measurements.isEmpty()) {//if (measurements != null) {
                // Include all the datapoints within the interval
                for (Value m : measurements) {
                    measurement = new JSONArray();
                    measurement.put(m.getTimeMeasured());
                    measurement.put(m.getValue());
                    pointsArray.put(measurement);
                }
                last = measurements.get(measurements.size() - 1).getTimeMeasured();
            }

            // Include the point describing the point at the end of the requested interval
            if (includeMissing) {
                measurement = new JSONArray();
                measurement.put(to.getTime());
                pointsArray.put(measurement);
            }
            jo.put("points", pointsArray);
            jo.put("unit", unit);
            jo.put("tz", timeZone);
            jo.put("last", last);
            jo.put("server_time", serverTime.getTime() / 1000);
            String result = jo.toString();
            return result;
        } catch (JSONException ex) {
            return null;
        }
    }

    private List<TestValue> generateLog(Long logLevel, Date from, Date to) {
        Random rnd = new Random();
        List<TestValue> values = new ArrayList<TestValue>();
        Long toTimeStamp = to.getTime();
        Long fromTimeSamp = from.getTime();
        while (fromTimeSamp < toTimeStamp) {
            TestValue value = new TestValue();
            value.setValue(rnd.nextInt(500));
            value.setTimeMeasured(fromTimeSamp);
            fromTimeSamp += logLevel * 5000;
            values.add(value);
        }
        return values;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private class TestValue {

        private Integer value;
        private Long timeMeasured;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public Long getTimeMeasured() {
            return timeMeasured;
        }

        public void setTimeMeasured(Long timeMeasured) {
            this.timeMeasured = timeMeasured;
        }

    }
}
